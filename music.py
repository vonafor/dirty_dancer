class MusicTypeEnum(object):
    RNB = 1
    ELECTROHOUSE = 2
    POP = 3


class Song(object):
    def __init__(self, title, type):
        self.title = title
        self.type = type
    
    def play(self):
        print '"{}" is playing now!'.format(self.title)
