from time import sleep

from music import MusicTypeEnum, Song
from move import HipHopMove, ElectroMove, PopMove
from dancer import Dancer


class Club(object):
    def __init__(self, listeners=None, songs=None):
        self.listeners = listeners or []
        self.songs = songs or []
    
    def hang_out(self):
        for song in self.songs:
            song.play()
            for listener in self.listeners:
                sleep(1)
                listener.on_song_change(song)
            sleep(3)
            print


if __name__ == '__main__':
    songs = [
        Song('Rockstar', MusicTypeEnum.POP),
        Song('U Blow My Mind', MusicTypeEnum.RNB),
        Song('Live The Night', MusicTypeEnum.ELECTROHOUSE),
        Song('I Know What You Want', MusicTypeEnum.RNB),
        Song('Faking It', MusicTypeEnum.POP),
        Song('Beautiful World', MusicTypeEnum.ELECTROHOUSE),
    ]

    dancers = [
        Dancer('Kim', sex='female', moves=[HipHopMove, ElectroMove]),
        Dancer('Mary', sex='female', moves=[PopMove]),
        Dancer('Nay'),
        Dancer('David', moves=[HipHopMove, ElectroMove, PopMove]),
    ]

    club = Club(listeners=dancers, songs=songs)
    club.hang_out()
