from music import MusicTypeEnum


class BaseMove(object):
    action = None
    type = None

    @classmethod
    def dance(cls, dancer):
        if not cls.action or not cls.type:
            raise NotImplementedError
        print "{}'s {}".format(dancer.name, cls.action)


class HipHopMove(BaseMove):
    action = 'raising hands'
    type = MusicTypeEnum.RNB


class ElectroMove(BaseMove):
    action = 'moving around'
    type = MusicTypeEnum.ELECTROHOUSE

class PopMove(BaseMove):
    action = 'shaking booty'
    type = MusicTypeEnum.POP
