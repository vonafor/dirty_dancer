class Dancer(object):
    def __init__(self, name, sex='male', moves=None):
        self.name = name
        self.sex = sex
        self.moves = moves or []
    
    def get_move(self, song):
        for move in self.moves:
            if move.type == song.type:
                return move
        return None
    
    def on_song_change(self, song):
        move = self.get_move(song)
        if move:
            move.dance(self)
        else:
            print "{}'s drinking spirits".format(self.name)
